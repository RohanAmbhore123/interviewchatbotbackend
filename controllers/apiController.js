var bodyParser = require('body-parser');
var apiai = require('apiai');
var cors = require('cors')

module.exports = function(app) {
    
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(cors())
    
    app.get('/get/', function(req, res) {
        
      res.send("hello");
    });
    
    app.post('/ai/', function(req, res) {
       var app = apiai("e412697a192548b3b86579a5375593b1");
      
        var request = app.textRequest(req.body.request_text, {
            sessionId: '6dda7bc5-112c-4f28-a54e-8acc517467bb'
        });
       
        request.on('response', function(response) {
            console.log(response);
            res.send(response);
        });

        request.on('error', function(error) {
            console.log(error);
            res.send(error);
        });
      request.end();
    });
}