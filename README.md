# InterviewChatBot


## Purpose and Responsibilities

**Do not edit this section.**

The purpose of this document is to ensure standardization on GitHub repository README documents. Copy and paste the entire text of this document as is, into any projects that do not already have this README template.  **Do not place passwords in this document!**

- **Development Leads** are responsible for filling out and keeping this document up to date during the course of the project. 
- **Development Leads** are responsible for ensuring that developers adheare to this document, specifically when it comes to the Features NOT to use during testing section. 
- **Project Managers** are responsible for ensuring that the Development Lead has completed this document before project delivery. 
- **Support Developers** are responsible for adhearing to this document, specifically when it comes to the Features NOT to use during testing section.

## App Overview
The purpose of the InterviewChatBotBackend(InterviewChatBot) is to genrate the API's for chatbot which is pointing to NLP/ML chatbot engine API.ai. This nodejs app is hosted on heroku cloud.

## Features NOT to use during testing
N/A

## Build Instructions
To setup the development enviroment:
npm install

## 3rd Party dependencies
APi.ai and Heroku cloud 
